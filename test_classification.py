from convert import Convert
from sklearn.externals import joblib


class TestClassification(object):

    def test_mocdel(self, folder, file, model_name):
        try:
            convert = Convert()
            clf_load = joblib.load(model_name)
            path = '{}/{}'.format(folder, file)

            print('Iniciando conversão e limpeza do arquivo')
            if 'html' in file:
                file_text = convert.convert_html_to_txt(path, '')[0]['noticia']
            else:
                file_text = convert.convert_pdf_to_txt(path, '')[0]['noticia']

            predicted = clf_load.predict([file_text])
            if round(predicted[0]) == 1:
                print('Arquivo enviado é de POLITICA')
            else:
               print('Arquivo enviado é de OUTROS')

            print('Precisão de {}%'.format(predicted [0] * 100 ))
        except Exception as exc:
            print('Erro ao testar modelo: {}'.format(str(exc)))

if __name__ == "__main__":
    print('Caminho da pasta (separado por /):')
    folder = input()
    print('Nome do arquivo com extensão (.pdf/.html)')
    file = input()

    path = "{}/{}".format(folder, file)
    print('Testando arquivo {}'.format(path))

    TestClassification().test_mocdel(folder, file, 'random_forest_politica.pkl')

