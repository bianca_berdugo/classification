import os
import tabula
from bs4 import BeautifulSoup
import re
from clean_text import CleanText
from lxml import html
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
import os
import pandas as pd

class Convert(object):

    def __init__(self):
        self.clean_text = CleanText()

    def convert_pdf_to_txt(self, path,label, pages=None):
        if not pages:
            pagenums = set()
        else:
            pagenums = set(pages)
        output = StringIO()
        manager = PDFResourceManager()
        converter = TextConverter(manager, output, laparams=LAParams())
        interpreter = PDFPageInterpreter(manager, converter)

        infile = open(path, 'rb')
        for page in PDFPage.get_pages(infile, pagenums):
            interpreter.process_page(page)
        infile.close()
        converter.close()
        text_clean = output.getvalue().replace('-\n','').replace('\n', ' ')
        text_split = text_clean.split('●')
        notices = []
        for text in text_split:
            text = text.lower().replace('próximo texto', '').replace('texto anterior', '').replace('índice', '')
            text = self.clean_text.master_clean(text)
            notices.append({'noticia': text, 'certo': label})

        output.close()
        return notices

    def convert_html_to_txt(self, path, label):
        with open(path, 'r', encoding='latin-1') as file:
            page = file.read()

        soup = BeautifulSoup(page, "lxml")

        table = soup.find_all('table')[4].text.replace('\n', ' ')

        table = table.lower().replace('próximo texto', '').replace('texto anterior', '').replace('índice', '')

        table = self.clean_text.master_clean(table)

        return [{'noticia': table, 'certo': label}]

    def convert_documents_into_df(self):
        documents=[]

        link = 'F:/Users/sug5824/Documents/PROJETOS/UNESP/2009-01/FSP'

        files = os.listdir('{}/{}'.format(link, path['name']))
        for file in files:
            try:
                print(file)
                name = '{}/{}/{}'.format(link, path['name'], file)
                if path['file'] == "pdf":
                    notices = self.convert_pdf_to_txt(name, path['type'])
                else:
                    notices = self.convert_html_to_txt(name, path['type'])

                documents += notices
            except Exception as exc:
                print('Read file error: {}'.format(str(exc)))

        return documents


if __name__ == '__main__':
    documents = []
    convert = Convert()
    paths = [{'name': 'negativo_train', 'type': 0, 'file': 'html', 'dst': 'mixed_train.pkl'},
             {'name': 'positivo_train', 'type': 1, 'file': 'pdf', 'dst': 'mixed_train.pkl'},
             {'name': 'negativo_test', 'type': 0, 'file': 'html', 'dst': 'mixed_test.pkl'},
             {'name': 'positivo_test', 'type': 1, 'file': 'pdf', 'dst': 'mixed_test.pkl'}]

    for path in paths:
        documents += convert.convert_documents_into_df()

    df = pd.DataFrame(data=documents, columns=['noticia', 'certo'])
    df.to_pickle('dataset.pkl')